# /etc/skel/.bash_profile

# This file is sourced by bash for login shells.  The following line
# runs your .bashrc and is recommended by the bash info pages.
[[ -f ~/.bashrc ]] && . ~/.bashrc

GPG_AGENT=/usr/bin/gpg-agent
${GPG_AGENT} --daemon
export GPG_AGENT_PID=`pidof ${GPG_AGENT}`

SSH_AGENT=/usr/bin/ssh-agent
SSH_AGENT_ARGS="-s"
eval `${SSH_AGENT} ${SSH_AGENT_ARGS}`

trap "kill ${GPG_AGENT_PID} ${SSH_AGENT_PID}" 0

export DESKTOP_SESSION="gnome"
export GTK2_RC_FILES="${HOME}/.gtkrc-2.0"
export QT_QPA_PLATFORMTHEME=gtk2
