#!/bin/bash

die() {
    echo "$@"
    exit 1
}

run() {
    echo "$@"
    $@ || die "$@ failed"
}

# Find out which directory the scripts are in.
scriptdir=$(realpath $(dirname $0))

# Make sure submodules are initialized.
run cd ${scriptdir}
run git submodule init
run git submodule update

# Initialize and update submodules in vim subdir.
pushd vim || die "pushd failed"
run git submodule init
run git submodule update
popd || die "popd failed"

# Symlink into home directory.
run ln -sf ${scriptdir}/vimrc ~/.vimrc
run rm -rf ~/.vim.bkp
mv ~/.vim ~/.vim.bkp
run ln -sf ${scriptdir}/vim ~/.vim
