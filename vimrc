let mapleader = ","

"" Initialize pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

"" Workaround the bug that WindowSwap's autoload is not autoloaded.
source ~/.vim/bundle/windowswap/autoload/WindowSwap.vim

"" Highlight vue files correctly, see https://github.com/posva/vim-vue
autocmd FileType vue syntax sync fromstart

"" https://github.com/flazz/vim-colorschemes 
colorscheme 256-grayvim

"" Enable project specific vimrc files.
set exrc
set secure

"" Enable line numbering
set number

"" Tabs are four spaces and nothing else.
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab

set textwidth=80
set colorcolumn=+1

"" Show whitespaces to not create horrible coding.
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set list
